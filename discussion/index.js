/*
	Objects - an object is a data type that is used to represent the specific details about a single concept
			- are created via curly braces {} and key-value pairs, separated by a colon
			- to the left of the colon, is the key, more commonly known as the property
			- to the right of the colon is the value
			- when using multiple key-value pairs, separate each one with a comma
			- objects can contain other objects, arrays and even functions
*/	

let person = {

	firstName: "John",
	lastName: "Smith",
	location: {
		city: "Tokyo",
		country: "Japan"
	},
	emails: ["john@mail.com", "johnsmith@mail.xyz"],

	brushTeeth: function(){
		console.log(this.firstName +" has brushed his teeth");
	}
}

// How to access an Object properties:
/*
	- using dot notation
*/

console.log(person.lastName);
console.log(person.emails[0]);
console.log(person.location.country);
person.brushTeeth();

// this refers to it's own object

// you can have empty objects
/*
Initializing/adding/editing object properties
*/

let car = {}

car.name = "Honda Civic";
car.manufactureDate = 2019;

console.log(car);


