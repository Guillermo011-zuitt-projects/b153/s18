trainer = {};

trainer.name = "Ash Ketchup";
trainer.age = 12;
trainer.pokemon = ['Pikachu', 'Charmander','Ghastly', 'Snorlax'];
trainer.friends = {
	hoenn: ['May', 'Kanto'],
	kanto: ['Brock', 'Misty']
};
trainer.talk = function(i){
	console.log(this.pokemon[i] + "! I choose you!");
}

console.log(trainer.name);
trainer.talk(3);